import 'dart:async';
import 'dart:convert' as json;

import 'package:conversor/model/unit.dart';
import 'package:flutter/material.dart';

final _appBarColor = Colors.orangeAccent;

class CategoryRoute extends StatefulWidget {
  const CategoryRoute();

  @override
  _CategoryRouteState createState() => _CategoryRouteState();
}

class _CategoryRouteState extends State<CategoryRoute> {
  final _categories = <Widget>[];
  static const _iconLocation = <IconData>[
    Icons.straighten,
    Icons.dashboard,
    Icons.call_to_action,
    Icons.battery_full,
    Icons.access_time,
    Icons.sd_card,
    Icons.flash_on,
  ];

  static const _baseColors = <Color>[
    Colors.orange,
    Colors.orange,
    Colors.orange,
    Colors.orange,
    Colors.orange,
    Colors.orange,
    Colors.orange,
    Colors.orange,
  ];

  Widget _buildCategoryRowWidgets(List<Widget> categories, int i) {
    int iTab = i * 4;
    if (iTab != 4) {
      // very hack, will found a better solution to fill row with flexible child,
      return Container(
          padding: EdgeInsets.all(4.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              categories[iTab],
              categories[iTab + 1],
              categories[iTab + 2],
              categories[iTab + 3],
            ],
          ));
    } else {
      return Container(
          padding: EdgeInsets.all(4.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              categories[iTab],
              Padding(
                padding: EdgeInsets.only(left: 36.0),
                child: categories[iTab + 1], 
              )
            ],
          ));
    }
  }

  Widget _buildCategoryWidgets(List<Widget> categories) {
    return ListView.builder(
      itemCount: categories.length ~/ 3,
      itemBuilder: (BuildContext context, int index) {
        return _buildCategoryRowWidgets(categories, index);
      },
    );
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();

    if (_categories.isEmpty) {
      await _retrieveLocalCategories();
    }
  }

  Future<void> _retrieveLocalCategories() async {
    final jsonStr = await DefaultAssetBundle.of(context)
        .loadString('assets/data/regular_units.json');
    final data = json.JsonDecoder().convert(jsonStr);
    if (data is! Map) {
      throw ('Data retrieved from API is not a Map');
    }
    var categoryIndex = 0;
    data.keys.forEach((key) {
      final List<Unit> units =
          data[key].map<Unit>((dynamic data) => Unit.fromJson(data)).toList();
      setState(() {
   //    _categories.add(category);
      });
      ++categoryIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_categories.isEmpty) {
      return Center(
        child: Container(
          height: 60.0,
          width: 60.0,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 15),
                LinearProgressIndicator()
              ],
            ),
          )
        ),
      );
    }

    final listView = Container(
      color: Colors.orangeAccent,
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      child: _buildCategoryWidgets(_categories),
    );

    final appBar = AppBar(
      elevation: 0.0,
      title: Text(
        'Conversor de unidades',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: _appBarColor,
    );

    return Scaffold(
      appBar: appBar,
      body: listView,
    );
  }
}
