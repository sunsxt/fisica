import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:conversor/model/category.dart';
import 'package:conversor/model/currency_provider.dart';
import 'package:conversor/model/unit.dart';
import 'package:conversor/screens/converter_screen.dart';
import 'package:conversor/widgets/backdrop.dart';
import 'package:conversor/widgets/category_tile.dart';

class CategoryScreen extends StatefulWidget {
  final CurrencyProvider currencyProvider;

  const CategoryScreen({this.currencyProvider});

  @override
  State<StatefulWidget> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  static const _icons = {
    'Unidades': 'assets/icons/length.png',
    'Area': 'assets/icons/area.png',
    'Volume': 'assets/icons/volume.png',
    'Masa': 'assets/icons/mass.png',
    'Time': 'assets/icons/time.png',
    'Digital Storage': 'assets/icons/digital_storage.png',
    'Energy': 'assets/icons/power.png',
    'Logou': 'assets/icons/logou.png'
  };

  final List<CategoryTile> _categories = <CategoryTile>[];

  Category _currentCategory;
  bool _isLoading = true;

  Future<void> _retrieveLocalCategories() async {
    final json =
        DefaultAssetBundle.of(context).loadString('assets/data/units.json');

    final data = JsonDecoder().convert(await json);

    if (data is! Map) {
      throw ('Data retrieved from assets is not a Map');
    }

    int categoryIndex = 0;

    // Create a [Category] with its list of [Unit]s from the JSON asset
    data.keys.forEach((key) {
      final List<Unit> units =
          data[key].map<Unit>((dynamic unit) => Unit.fromJson(unit)).toList();

      // The state needs to be updated to make sure orientation changes work
      // as well as the unit converter selection via the [Backdrop]
      setState(() {
        this._categories.add(CategoryTile(
              category: Category(
                name: key,
                color: Colors.orangeAccent,
                units: units,
                iconLocation: _icons[key],
              ),
              onTapHandler: this._onCategoryTap,
            ));
      });

      categoryIndex++;
    });

    setState(() {
      this._currentCategory = this._categories[0].category;
    });
  }

  /// Retrieves the units for the Currency [Category] from an external provider
  Future<void> _retrieveCurrencyCategory() async {
    bool shouldBeDisabled = false;
    List<Unit> units = [];

    try {
      units = await widget.currencyProvider.getUnits();
    } catch (e) {
      print('Caught an exception while retrieving Currency units: $e');
      shouldBeDisabled = true;
    }
  }

  Widget _buildCategoryWidgets(Orientation orientation) {
    if (orientation == Orientation.portrait) {
      return ListView(
        children: this._categories,
      );
    } else {
      return GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 3.0,
        children: this._categories,
      );
    }
  }

  void _onCategoryTap(Category category) {
    setState(() {
      this._currentCategory = category;
    });
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    if (this._categories.isEmpty) {
      await this._retrieveLocalCategories();
      await this._retrieveCurrencyCategory();

      setState(() {
        this._isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (this._isLoading) {
      return Scaffold(
        body: Center(
          child: Container(
              height: 60.0,
              width: 60.0,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(height: 15),
                    LinearProgressIndicator()
                  ],
                ),
              )),
        ),
      );
    }

    final categoriesView = Padding(
      child: this._buildCategoryWidgets(MediaQuery.of(context).orientation),
      padding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 48.0,
      ),
    );

    return Backdrop(
      currentCategory: this._currentCategory,
      frontPanel: ConverterScreen(
        category: this._currentCategory,
        currencyProvider: widget.currencyProvider,
      ),
      backPanel: categoriesView,
      frontTitle: Text('Conversor de unidades', style: TextStyle(fontSize: 16)),
      backTitle: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Selecciona una opción', style: TextStyle(fontSize: 16)),
          Image.asset(
            'assets/icons/logou.png',
            width: 120,
          )
        ],
      ),
    );
  }
}
