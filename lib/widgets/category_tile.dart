import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:conversor/model/category.dart';

class CategoryTile extends StatelessWidget {
  final Category category;
  final ValueChanged<Category> onTapHandler;
  final bool disabled;

  const CategoryTile({
    @required this.category,
    @required this.onTapHandler,
    this.disabled = false,
  }) : assert(category != null),
       assert(onTapHandler != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.orangeAccent,
      height: 100.0,
      child: InkWell(
        highlightColor: Colors.orangeAccent,
        splashColor: Colors.orangeAccent,
        borderRadius: BorderRadius.all(Radius.circular(50.0)),
        // Make this [CategoryTile] not tap-able when disabled
        onTap: this.disabled ? null : () => this.onTapHandler(this.category),
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Image.asset(
                    this.category.iconLocation,
                  ),
                ),
                Text(
                  this.category.name,
                  style: TextStyle(
                    fontSize: 24.0,
                    // Make this [CategoryTile] look disabled by greying it out
                    color: this.disabled ? Colors.grey[600] : Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}